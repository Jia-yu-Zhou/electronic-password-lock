#include "key.h"
#include "delay.h"
#include "code.h"
#include "oled.h"
u8 key_value = 0;
u8 key_char_value = 0;
void GPIOA_set0()
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_ResetBits(GPIOA, GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7);
}

void GPIOA_set1()
{
    GPIO_InitTypeDef GPIO_InitStructure;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_ResetBits(GPIOA, GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3);
}

void EXTI_ENABLE()
{
    EXTI_InitTypeDef EXTI_InitStructure;

    EXTI_InitStructure.EXTI_Line = EXTI_Line0 | EXTI_Line1 | EXTI_Line2 | EXTI_Line3;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
}

void EXTI_DISABLE()
{
    EXTI_InitTypeDef EXTI_InitStructure;

    EXTI_InitStructure.EXTI_Line = EXTI_Line0 | EXTI_Line1 | EXTI_Line2 | EXTI_Line3;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
    EXTI_InitStructure.EXTI_LineCmd = DISABLE;
    EXTI_Init(&EXTI_InitStructure);
}

void key_Init()
{
    NVIC_InitTypeDef NVIC_InitStructure;

    GPIOA_set0();

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

    GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource0);
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource1);
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource2);
    GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource3);

    EXTI_ENABLE();

    NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x01;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = EXTI2_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    NVIC_InitStructure.NVIC_IRQChannel = EXTI3_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x03;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

extern int TIM3_cnt2;
void EXTI0_IRQHandler(void)
{
    delay_ms(10);
    TIM3_cnt2 = 0;
    if (line0 == 0)
    {
        EXTI_DISABLE();
        GPIOA_set1();
        if (list0 == 0)
        {
            key_value = 1;
            key_char_value = '7';
        }
        else if (list1 == 0)
        {
            key_value = 5;
            key_char_value = '8';
        }
        else if (list2 == 0)
        {
            key_value = 9;
            key_char_value = '9';
        }
        else if (list3 == 0)
            key_value = 13;
        while (list0 == 0 || list1 == 0 || list2 == 0 || list3 == 0)
        {
        }
    }
    EXTI_ClearITPendingBit(EXTI_Line0);
    key_Init();
}

void EXTI1_IRQHandler(void)
{
    TIM3_cnt2 = 0;
    delay_ms(10);
    if (line1 == 0)
    {
        EXTI_DISABLE();
        GPIOA_set1();
        if (list0 == 0)
        {
            key_value = 2;
            key_char_value = '4';
        }
        else if (list1 == 0)
        {
            key_value = 6;
            key_char_value = '5';
        }
        else if (list2 == 0)
        {
            key_value = 10;
            key_char_value = '6';
        }
        else if (list3 == 0)
            key_value = 14;
        while (list0 == 0 || list1 == 0 || list2 == 0 || list3 == 0)
        {
        }
    }
    EXTI_ClearITPendingBit(EXTI_Line1);
    key_Init();
}

void EXTI2_IRQHandler(void)
{
    TIM3_cnt2 = 0;
    delay_ms(10);
    if (line2 == 0)
    {
        EXTI_DISABLE();
        GPIOA_set1();
        if (list0 == 0)
        {
            key_value = 3;
            key_char_value = '1';
        }
        else if (list1 == 0)
        {
            key_value = 7;
            key_char_value = '2';
        }
        else if (list2 == 0)
        {
            key_value = 11;
            key_char_value = '3';
        }
        else if (list3 == 0)
            key_value = 15;
        while (list0 == 0 || list1 == 0 || list2 == 0 || list3 == 0)
        {
        }
    }
    EXTI_ClearITPendingBit(EXTI_Line2);
    key_Init();
}

void EXTI3_IRQHandler(void)
{
    TIM3_cnt2 = 0;
    delay_ms(10);
    if (line3 == 0)
    {
        EXTI_DISABLE();
        GPIOA_set1();
        if (list0 == 0)
            key_value = 4;
        else if (list1 == 0)
        {
            key_value = 8;
            key_char_value = '0';
        }
        else if (list2 == 0)
            key_value = 12;
        else if (list3 == 0)
            key_value = 16;
        while (list0 == 0 || list1 == 0 || list2 == 0 || list3 == 0)
        {
        }
    }
    EXTI_ClearITPendingBit(EXTI_Line3);
    key_Init();
}
