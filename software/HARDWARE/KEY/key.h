#ifndef __KEY_H
#define __KEY_H

#include "sys.h"

extern u8 key_value;
extern u8 key_char_value;

#define line0 GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0)
#define line1 GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_1)
#define line2 GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_2)
#define line3 GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_3)

#define list0 GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_4)
#define list1 GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_5)
#define list2 GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_6)
#define list3 GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_7)

void GPIOA_set0(void);
void GPIOA_set1(void);
void EXTI_ENABLE(void);
void EXTI_DISABLE(void);
void key_Init(void);

#endif
