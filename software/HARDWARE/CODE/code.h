#ifndef __CODE_H
#define __CODE_H

#define code_lenth 8

typedef struct
{
    char mode;
    char code_state;
    char opera_mode;
    char code_buff[code_lenth];
    char code_saved[4][code_lenth];
    int code_len;
} code_struct;

extern code_struct CODE;

enum // mode
{
    lock,
    unlock
};

enum
{
    code_exist,
    code_null
};

enum
{
    select_CODE1,
    select_CODE2,
    select_CODE3,
    change_CODE1,
    change_CODE2,
    change_CODE3
};

void code_input(void);
void code_read(void);
char code_strcmp(char *p, char *q);
void code_start_check(void);
#endif
