#include "code.h"
#include "key.h"
#include "oled.h"
#include "24cxx.h"
#include "string.h"
#include "delay.h"
code_struct CODE;
char erro_num = 0;
void code_input(void)
{
    int i, code_check;

    /* MODE */
    if (CODE.mode == lock)
    {
        OLED_ShowString(20, 0, "Input Code");
    }
    else if (CODE.mode == unlock)
    {
        if (CODE.opera_mode == select_CODE1)
        {
            OLED_ShowString(35, 1, ">> CODE1");
            OLED_ShowString(35, 3, "   CODE2");
            OLED_ShowString(35, 5, "   CODE3");
            if (key_value == 16) // press ok
            {
                OLED_Clear();
                key_value = 0;
                CODE.opera_mode = change_CODE1;
            }
        }
        else if (CODE.opera_mode == select_CODE2)
        {
            OLED_ShowString(35, 1, "   CODE1");
            OLED_ShowString(35, 3, ">> CODE2");
            OLED_ShowString(35, 5, "   CODE3");
            if (key_value == 16) // press ok
            {
                OLED_Clear();
                key_value = 0;
                CODE.opera_mode = change_CODE2;
            }
        }
        else if (CODE.opera_mode == select_CODE3)
        {
            OLED_ShowString(35, 1, "   CODE1");
            OLED_ShowString(35, 3, "   CODE2");
            OLED_ShowString(35, 5, ">> CODE3");
            if (key_value == 16) // press ok
            {
                key_value = 0;
                OLED_Clear();
                CODE.opera_mode = change_CODE3;
            }
        }

        if (CODE.opera_mode == change_CODE1)
        {
            OLED_ShowString(18, 1, "Input code");
            if (key_value == 16 && CODE.code_len == 8)
            {
                key_value = 0;
                OLED_Clear();
                for (i = 0; i < code_lenth; i++)
                {
                    AT24CXX_WriteLenByte(i + 8 * 0, CODE.code_buff[i], 1);
                    OLED_ShowString(18, 1, "save code...");
                }
                delay_ms(500);
                OLED_Clear();
                OLED_ShowString(18, 1, "save code OK");
                memset(CODE.code_buff, 1, 8);
                delay_ms(1000);
                OLED_Clear();
                CODE.opera_mode = select_CODE1;
                CODE.code_len = 0;
            }
        }
        else if (CODE.opera_mode == change_CODE2)
        {
            OLED_ShowString(18, 1, "Input code");
            if (key_value == 16 && CODE.code_len == 8)
            {
                key_value = 0;
                OLED_Clear();
                for (i = 0; i < code_lenth; i++)
                {
                    AT24CXX_WriteLenByte(i + 8 * 1, CODE.code_buff[i], 1);
                    OLED_ShowString(18, 1, "save code...");
                }
                delay_ms(500);
                OLED_Clear();
                OLED_ShowString(18, 1, "save code OK");
                memset(CODE.code_buff, 1, 8);
                delay_ms(1000);
                OLED_Clear();
                CODE.opera_mode = select_CODE1;
                CODE.code_len = 0;
            }
        }
        else if (CODE.opera_mode == change_CODE3)
        {
            OLED_ShowString(18, 1, "Input code");
            if (key_value == 16 && CODE.code_len == 8)
            {
                key_value = 0;
                OLED_Clear();
                for (i = 0; i < code_lenth; i++)
                {
                    AT24CXX_WriteLenByte(i + 8 * 2, CODE.code_buff[i], 1);
                    OLED_ShowString(18, 1, "save code...");
                }
                delay_ms(500);
                OLED_Clear();
                OLED_ShowString(18, 1, "save code OK");
                memset(CODE.code_buff, 1, 8);
                delay_ms(1000);
                OLED_Clear();
                CODE.opera_mode = select_CODE1;
                CODE.code_len = 0;
            }
        }
    }

    /* KEY */
    if (key_char_value != 0 && key_char_value >= '0' && key_char_value <= '9' && CODE.code_len < code_lenth)
    {
        if (CODE.mode == lock || CODE.opera_mode == change_CODE1 || CODE.opera_mode == change_CODE2 || CODE.opera_mode == change_CODE3)
        {
            CODE.code_buff[CODE.code_len] = key_char_value;
            if (CODE.mode == unlock)
                OLED_ShowChar(9 + CODE.code_len * 16, 3, CODE.code_buff[CODE.code_len]);
            else
                OLED_ShowChar(9 + CODE.code_len * 16, 3, '*');
            CODE.code_len++;
        }
        key_char_value = 0;
        key_value = 0;
    }
    else if (key_value == 15) // press DEL
    {
        key_value = 0;
        key_char_value = 0;
        if (CODE.code_len > 0)
        {
            OLED_ShowChar(9 + (CODE.code_len - 1) * 16, 3, ' ');
            CODE.code_buff[CODE.code_len - 1] = '\0';
            CODE.code_len--;
        }
        if (CODE.opera_mode == select_CODE1 && CODE.mode == unlock)
        {
            OLED_Clear();
            for (i = 0; i < code_lenth; i++)
            {
                AT24CXX_WriteLenByte(i + 8 * 0, '\0', 1);
                OLED_ShowString(0, 3, "Delet CODE1...");
            }
            delay_ms(500);
            OLED_Clear();
            OLED_ShowString(0, 3, "Delet CODE1 OK");
            delay_ms(900);
            OLED_Clear();
        }
        else if (CODE.opera_mode == select_CODE2 && CODE.mode == unlock)
        {
            OLED_Clear();
            for (i = 0; i < code_lenth; i++)
            {
                AT24CXX_WriteLenByte(i + 8 * 1, '\0', 1);
                OLED_ShowString(0, 3, "Delet CODE2...");
            }
            delay_ms(500);
            OLED_Clear();
            OLED_ShowString(0, 3, "Delet CODE2 OK");
            delay_ms(900);
            OLED_Clear();
        }
        else if (CODE.opera_mode == select_CODE3 && CODE.mode == unlock)
        {
            OLED_Clear();
            for (i = 0; i < code_lenth; i++)
            {
                AT24CXX_WriteLenByte(i + 8 * 2, '\0', 1);
                OLED_ShowString(0, 3, "Delet CODE3...");
            }
            delay_ms(500);
            OLED_Clear();
            OLED_ShowString(0, 3, "Delet CODE3 OK");
            delay_ms(900);
            OLED_Clear();
        }
    }
    else if (key_value == 16) // press OK,input finish and save the code to EEPROM
    {
        key_value = 0;
        key_char_value = 0;
        if (CODE.mode == lock)
        {
            code_check = code_strcmp(CODE.code_buff, CODE.code_saved[0]) |
                         code_strcmp(CODE.code_buff, CODE.code_saved[1]) |
                         code_strcmp(CODE.code_buff, CODE.code_saved[2]) |
                         code_strcmp(CODE.code_buff, CODE.code_saved[3]);
            if (code_check)
            {
                erro_num = 0;
                CODE.mode = unlock;
                OLED_Clear();
                OLED_ShowString(20, 0, "Input Code");
                OLED_ShowString(60, 5, "Correct!");
                CODE.opera_mode = select_CODE1;
            }
            else if (!code_check)
            {
                erro_num++;
                OLED_Clear();
                OLED_ShowString(20, 0, "Input Code");
                if (erro_num < 3)
                    OLED_ShowString(60, 5, "Error!");
                else
                    OLED_ShowString(50, 5, "Warning!");
            }
            memset(CODE.code_buff, 1, 8);
            CODE.code_len = 0;
            delay_ms(1000);
            OLED_Clear();
        }
        if (CODE.code_len == 8)
        {
            memset(CODE.code_buff, 1, 8);
            CODE.code_len = 0;
        }
    }
    else if (key_value == 14 && CODE.mode == unlock) // press down
    {
        key_value = 0;
        key_char_value = 0;
        if (CODE.opera_mode == select_CODE1)
        {
            OLED_Clear();
            CODE.opera_mode = select_CODE2;
        }
        else if (CODE.opera_mode == select_CODE2)
        {
            OLED_Clear();
            CODE.opera_mode = select_CODE3;
        }
        else if (CODE.opera_mode == select_CODE3)
        {
            OLED_Clear();
            CODE.opera_mode = select_CODE1;
        }
    }

    else if (key_value == 13 && CODE.mode == unlock) // press up
    {
        key_value = 0;
        key_char_value = 0;
        if (CODE.opera_mode == select_CODE1)
        {
            OLED_Clear();
            CODE.opera_mode = select_CODE3;
        }
        else if (CODE.opera_mode == select_CODE3)
        {
            OLED_Clear();
            CODE.opera_mode = select_CODE2;
        }
        else if (CODE.opera_mode == select_CODE2)
        {
            OLED_Clear();
            CODE.opera_mode = select_CODE1;
        }
    }
}

void code_read(void)
{
    int i = 0, j = 0;
    for (j = 0; j < 4; j++)
    {
        for (i = 0; i < code_lenth; i++)
        {
            CODE.code_saved[j][i] = (char)AT24CXX_ReadLenByte(i + j * 8, 1);
        }
    }
}

char code_strcmp(char *p, char *q)
{
    int i = 0;
    while (i < 8)
    {
        if (*p == *q)
        {
            *p++;
            *q++;
        }
        else
            return 0;
        i++;
    }
    return 1;
}
