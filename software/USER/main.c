#include "delay.h"
#include "sys.h"
#include "oled.h"
#include "bmp.h"
#include "code.h"
#include "key.h"
#include "24cxx.h"
#include "led.h"
#include "string.h"
extern int overtime;
char root_code[8] = "19002408";
int main(void)
{
	int i;
	delay_init();
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	LED_Init();
	OLED_Init();
	key_Init();
	AT24CXX_Init();
	OLED_Clear();

	delay_ms(500);
	CODE.mode = lock; // lock
	memset(CODE.code_buff, 1, 8);
	for (i = 0; i < code_lenth; i++)
	{
		AT24CXX_WriteLenByte(i + 8 * 3, root_code[i], 1);
	}
	code_read();
	while (1)
	{
		code_input();
		code_read();
		if (overtime == 1)
		{
			OLED_Clear();
			overtime = 0;
		}
	}
}
